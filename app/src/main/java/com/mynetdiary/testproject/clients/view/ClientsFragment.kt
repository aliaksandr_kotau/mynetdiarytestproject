package com.mynetdiary.testproject.clients.view

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.mynetdiary.testproject.R
import com.mynetdiary.testproject.databinding.FragmentClientsBinding
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class ClientsFragment : Fragment(R.layout.fragment_clients) {

    private val clientsSharedViewModel by sharedViewModel<ClientsSharedViewModel>()
    private val clientsViewModel by viewModel<ClientsViewModel> {
        parametersOf(clientsSharedViewModel, findNavController())
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val binding = FragmentClientsBinding.bind(view)
        binding.viewModel = clientsViewModel

        val adapter = ClientsAdapter()
        binding.clients.adapter = adapter

        clientsViewModel.clients.observe(viewLifecycleOwner, { clients ->
            adapter.submitList(clients)
        })
    }
}