package com.mynetdiary.testproject.clients.di

import androidx.navigation.NavController
import com.mynetdiary.testproject.clients.view.ClientsSharedViewModel
import com.mynetdiary.testproject.clients.view.ClientsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val clientsModule = module {
    viewModel { (clientsSharedViewModel: ClientsSharedViewModel, navController: NavController) ->
        ClientsViewModel(
            clientsSharedViewModel = clientsSharedViewModel,
            navController = navController
        )
    }

    viewModel { ClientsSharedViewModel() }
}