package com.mynetdiary.testproject.clients.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.mynetdiary.testproject.BR
import com.mynetdiary.testproject.R
import com.mynetdiary.testproject.model.Client

class ClientsAdapter : ListAdapter<ClientListItem, ClientsViewHolder>(ClientsDiffItemCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ClientsViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ViewDataBinding>(layoutInflater, R.layout.list_item_client, parent, false)
        return ClientsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ClientsViewHolder, position: Int) = holder.bind(getItem(position))
}

class ClientsViewHolder(val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(clientListItem: ClientListItem) {
        binding.setVariable(BR.clientListItem, clientListItem)
        binding.executePendingBindings()
    }
}

class ClientsDiffItemCallback : DiffUtil.ItemCallback<ClientListItem>() {

    override fun areItemsTheSame(oldItem: ClientListItem, newItem: ClientListItem) = oldItem.client.id == newItem.client.id

    override fun areContentsTheSame(oldItem: ClientListItem, newItem: ClientListItem) =
        oldItem.client.weight == newItem.client.weight
                && oldItem.client.dateOfBirth == newItem.client.dateOfBirth
                && oldItem.client.photoUri == newItem.client.photoUri
}

data class ClientListItem(val client: Client, val onClientClick: () -> Unit) {

    fun onClientClick() {
        onClientClick.invoke()
    }
}