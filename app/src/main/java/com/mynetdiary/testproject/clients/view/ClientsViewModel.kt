package com.mynetdiary.testproject.clients.view

import androidx.databinding.Observable
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.navigation.NavController
import com.mynetdiary.testproject.R
import com.mynetdiary.testproject.clientdetails.view.ClientDetailsFragmentDirections

class ClientsViewModel(val clientsSharedViewModel: ClientsSharedViewModel,
                       private val navController: NavController) : ViewModel() {

    private val clientsDataChangedCallback = object : Observable.OnPropertyChangedCallback() {
        override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
            clientsSharedViewModel.clientsData.get()?.let { clients ->
                _clients.value = clients.map {
                    ClientListItem(it) {
                        val action = ClientDetailsFragmentDirections.openClientDetailsFragment(it)
                        navController.navigate(action)
                    }
                }
            }
        }
    }

    private val _clients by lazy { MutableLiveData<List<ClientListItem>>() }
    val clients = _clients as LiveData<List<ClientListItem>>

    init {
        clientsSharedViewModel.clientsData.set(listOf())
        clientsSharedViewModel.clientsData.addOnPropertyChangedCallback(clientsDataChangedCallback)
    }

    fun onNewClientClick() {
        navController.navigate(R.id.openClientDetailsFragment)
    }

    override fun onCleared() {
        super.onCleared()
        clientsSharedViewModel.clientsData.removeOnPropertyChangedCallback(clientsDataChangedCallback)
        clientsSharedViewModel.clientsData.set(null)
    }
}