package com.mynetdiary.testproject.clients.view

import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.mynetdiary.testproject.model.Client

class ClientsSharedViewModel : ViewModel() {
    val clientsData = ObservableField<List<Client>>()
}