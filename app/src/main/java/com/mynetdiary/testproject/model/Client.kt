package com.mynetdiary.testproject.model

import android.os.Parcelable
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.mynetdiary.testproject.BR
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
class Client(
    val id: String = UUID.randomUUID().toString(),
    private var _weight: String? = null,
    private var _dateOfBirth: String? = null,
    private var _photoUri: String? = null
) : Parcelable, BaseObservable() {

    var weight: String?
    @Bindable get() = _weight
    set(value) {
        _weight = value
        notifyPropertyChanged(BR.weight)
    }

    var dateOfBirth: String?
        @Bindable get() = _dateOfBirth
        set(value) {
            _dateOfBirth = value
            notifyPropertyChanged(BR.dateOfBirth)
        }

    var photoUri: String?
        @Bindable get() = _photoUri
        set(value) {
            _photoUri = value
            notifyPropertyChanged(BR.photoUri)
        }
}