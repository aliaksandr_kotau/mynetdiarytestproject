package com.mynetdiary.testproject.core.utils

import android.content.Context
import androidx.annotation.ArrayRes
import androidx.annotation.StringRes

class ResUtils(private val context: Context) {

    fun getStringArray(@ArrayRes resId: Int): Array<String> = context.resources.getStringArray(resId)
}