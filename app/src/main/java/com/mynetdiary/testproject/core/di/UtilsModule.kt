package com.mynetdiary.testproject.core.di

import com.mynetdiary.testproject.core.utils.ResUtils
import com.mynetdiary.testproject.core.utils.StorageUtils
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val utilsModule = module {
    single { ResUtils(androidContext()) }
    single { StorageUtils(androidContext()) }
}