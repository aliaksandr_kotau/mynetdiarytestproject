package com.mynetdiary.testproject.core.utils

import android.content.Context
import android.net.Uri
import android.os.Environment
import androidx.core.content.FileProvider
import java.io.*
import java.text.SimpleDateFormat
import java.util.*

private const val MY_NET_DIARY_TEST_PROJECT_SUB_DIRECTORY = "MyNetDiaryTestProject"

class StorageUtils(private val context: Context) {

    @Throws(IOException::class)
    fun createExternalAppStorageImageFile(): Uri {
        return createExternalStorageFile().uri()
    }

    @Throws(IOException::class, FileNotFoundException::class)
    fun copyImageToExternalAppStorage(uri: Uri): Uri {
        val inputFd = context.contentResolver.openFileDescriptor(uri, "r")
        val tempImageFile = createExternalStorageFile()
        if (inputFd != null) {
            val outputStream = FileOutputStream(tempImageFile)
            FileInputStream(inputFd.fileDescriptor).channel.use { inputChannel ->
                outputStream.channel.use { outputChannel ->
                    outputChannel.transferFrom(inputChannel, 0, inputChannel.size())
                }
            }
        } else throw IOException("Parcel file descriptor for the Uri $uri is null")

        return tempImageFile.uri()
    }

    @Throws(IOException::class)
    private fun createExternalStorageFile(): File {
        val imageFileName = "IMG_${SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(Date())}_"
        val storageDir = File(context.getExternalFilesDir(Environment.DIRECTORY_PICTURES), MY_NET_DIARY_TEST_PROJECT_SUB_DIRECTORY)
        return if (storageDir.exists() || storageDir.mkdirs()) {
            File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",  /* suffix */
                storageDir /* directory */
            )
        } else throw IOException("Cannot create a sub-directory with the path $storageDir")
    }

    private fun File.uri(): Uri {
        return FileProvider.getUriForFile(
            context,
            "${context.packageName}.fileprovider",
            this
        )
    }
}