package com.mynetdiary.testproject.clientdetails.view.weight

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.mynetdiary.testproject.R
import com.mynetdiary.testproject.clientdetails.view.ClientDetailsSharedViewModel
import com.mynetdiary.testproject.databinding.FragmentWeightBinding
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class WeightFragment : Fragment(R.layout.fragment_weight) {

    companion object {
        fun newInstance() = WeightFragment()
    }

    private val clientDetailsSharedViewModel by sharedViewModel<ClientDetailsSharedViewModel>()
    private val weightViewModel by viewModel<WeightViewModel> {
        parametersOf(clientDetailsSharedViewModel, findNavController())
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val binding = FragmentWeightBinding.bind(view)
        binding.viewModel = weightViewModel
    }
}