package com.mynetdiary.testproject.clientdetails.view.dateofbirth

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.google.android.material.datepicker.MaterialDatePicker
import com.mynetdiary.testproject.R
import com.mynetdiary.testproject.clientdetails.view.ClientDetailsSharedViewModel
import com.mynetdiary.testproject.databinding.FragmentDateOfBirthBinding
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import java.util.*

class DateOfBirthFragment : Fragment(R.layout.fragment_date_of_birth) {

    companion object {
        fun newInstance() = DateOfBirthFragment()
    }

    private val clientDetailsSharedViewModel by sharedViewModel<ClientDetailsSharedViewModel>()
    private val dateOfBirthViewModel by viewModel<DateOfBirthViewModel> {
        parametersOf(clientDetailsSharedViewModel)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val binding = FragmentDateOfBirthBinding.bind(view)
        binding.viewModel = dateOfBirthViewModel

        binding.dateOfBirth.setOnClickListener {
            showDatePicker()
        }
    }

    private fun showDatePicker() {
        val datePicker = MaterialDatePicker.Builder.datePicker()
            .setTitleText(getString(R.string.date_of_birth_picker_title))
            .setSelection(MaterialDatePicker.todayInUtcMilliseconds()).build()

        datePicker.addOnPositiveButtonClickListener {
            dateOfBirthViewModel.onDateOfBirthSelected(Date(it))
        }

        datePicker.show(parentFragmentManager, "")
    }
}