package com.mynetdiary.testproject.clientdetails.view.bindingadapters

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import androidx.databinding.BindingAdapter

@BindingAdapter("app:onTextChanged")
fun EditText.onTextChanged(onTextChanged: OnTextChanged?) {
    addTextChangedListener(object : TextWatcher {
        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
            onTextChanged?.onTextChanged(s.toString())
        }
        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
        override fun afterTextChanged(s: Editable) {}
    })
}

interface OnTextChanged {
    fun onTextChanged(s: String)
}