package com.mynetdiary.testproject.clientdetails.view

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter

class ClientDetailsAdapter(private val fragmentList: List<Fragment>,
                           hostFragment: Fragment) : FragmentStateAdapter(hostFragment) {

    override fun getItemCount() = fragmentList.size

    override fun createFragment(position: Int) = fragmentList[position]
}