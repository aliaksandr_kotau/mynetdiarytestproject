package com.mynetdiary.testproject.clientdetails.view.bindingadapters

import androidx.databinding.BindingAdapter
import androidx.viewpager2.widget.ViewPager2

@BindingAdapter("app:selectedPage")
fun ViewPager2.selectedPage(selectedPage: Int) {
    if (currentItem != selectedPage) {
        setCurrentItem(selectedPage, true)
    }
}

@BindingAdapter("app:isUserInputEnabled")
fun ViewPager2.isUserInputEnabled(isUserInputEnabled: Boolean) {
    this.isUserInputEnabled = isUserInputEnabled
}