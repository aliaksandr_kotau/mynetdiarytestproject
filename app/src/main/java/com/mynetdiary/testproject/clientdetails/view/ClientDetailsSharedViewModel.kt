package com.mynetdiary.testproject.clientdetails.view

import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import com.mynetdiary.testproject.model.Client

class ClientDetailsSharedViewModel : ViewModel() {
    val clientData = ObservableField<Client>()
    val selectedPage = ObservableField<Int>()
}