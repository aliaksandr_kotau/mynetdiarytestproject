package com.mynetdiary.testproject.clientdetails.view.bindingadapters

import android.view.View
import android.widget.AdapterView
import android.widget.Spinner
import androidx.databinding.BindingAdapter
import com.mynetdiary.testproject.R

@BindingAdapter("app:selectedEntry")
fun Spinner.selectedEntry(entry: String?) {
    entry?.let {
        val entries = resources.getStringArray(R.array.weight_dimensions)
        setSelection(entries.indexOf(it))
    }
}

@BindingAdapter("app:onEntrySelected")
fun Spinner.onEntrySelected(onEntrySelectedListener: OnEntrySelectedListener?) {
    onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            onEntrySelectedListener?.onEntrySelected(resources.getStringArray(R.array.weight_dimensions)[position])
        }

        override fun onNothingSelected(parent: AdapterView<*>?) {}
    }
}

interface OnEntrySelectedListener {
    fun onEntrySelected(entry: String)
}