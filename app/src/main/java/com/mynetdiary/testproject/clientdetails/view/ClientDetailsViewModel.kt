package com.mynetdiary.testproject.clientdetails.view

import androidx.lifecycle.ViewModel
import com.mynetdiary.testproject.model.Client

class ClientDetailsViewModel(client: Client?,
                             val clientDetailsSharedViewModel: ClientDetailsSharedViewModel) : ViewModel() {

    init {
        clientDetailsSharedViewModel.clientData.set(client ?: Client())
        clientDetailsSharedViewModel.selectedPage.set(0)
    }

    override fun onCleared() {
        super.onCleared()
        clientDetailsSharedViewModel.clientData.set(null)
        clientDetailsSharedViewModel.selectedPage.set(null)
    }
}