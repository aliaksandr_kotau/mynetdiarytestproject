package com.mynetdiary.testproject.clientdetails.view.weight

import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel
import androidx.navigation.NavController
import com.mynetdiary.testproject.R
import com.mynetdiary.testproject.clientdetails.view.ClientDetailsSharedViewModel
import com.mynetdiary.testproject.core.utils.ResUtils

class WeightViewModel(private val clientDetailsSharedViewModel: ClientDetailsSharedViewModel,
                      resUtils: ResUtils,
                      private val navController: NavController) : ViewModel() {

    var weight = clientDetailsSharedViewModel.clientData.get()?.weight?.split(" ")?.get(0)
    var weightDimension: String = clientDetailsSharedViewModel.clientData.get()?.weight?.split(" ")?.get(1)
        ?: resUtils.getStringArray(R.array.weight_dimensions)[0]

    val isNextButtonEnabled = ObservableField<Boolean>()

    fun onWeightChanged(weight: String) {
        this.weight = weight
        clientDetailsSharedViewModel.clientData.get()?.weight = "$weight $weightDimension"

        isNextButtonEnabled.set(!this.weight.isNullOrBlank())
    }

    fun onWeightDimensionChanged(weightDimension: String) {
        this.weightDimension = weightDimension
        clientDetailsSharedViewModel.clientData.get()?.weight = "$weight $weightDimension"
    }

    fun onBackClick() {
        navController.navigateUp()
    }

    fun onNextClick() {
        clientDetailsSharedViewModel.selectedPage.get()?.let { selectedPage ->
            clientDetailsSharedViewModel.selectedPage.set(selectedPage + 1)
        }
    }
}