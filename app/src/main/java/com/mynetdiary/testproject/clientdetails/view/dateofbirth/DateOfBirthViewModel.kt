package com.mynetdiary.testproject.clientdetails.view.dateofbirth

import androidx.lifecycle.ViewModel
import com.mynetdiary.testproject.clientdetails.view.ClientDetailsSharedViewModel
import java.text.SimpleDateFormat
import java.util.*

class DateOfBirthViewModel(val clientDetailsSharedViewModel: ClientDetailsSharedViewModel) : ViewModel() {

    fun onDateOfBirthSelected(dateOfBirth: Date) {
        val dateFormat = SimpleDateFormat("MMM dd, yyyy", Locale.getDefault())
        clientDetailsSharedViewModel.clientData.get()?.dateOfBirth = dateFormat.format(dateOfBirth)
    }

    fun onBackClick() {
        clientDetailsSharedViewModel.selectedPage.get()?.let { selectedPage ->
            clientDetailsSharedViewModel.selectedPage.set(selectedPage - 1)
        }
    }

    fun onNextClick() {
        clientDetailsSharedViewModel.selectedPage.get()?.let { selectedPage ->
            clientDetailsSharedViewModel.selectedPage.set(selectedPage + 1)
        }
    }
}