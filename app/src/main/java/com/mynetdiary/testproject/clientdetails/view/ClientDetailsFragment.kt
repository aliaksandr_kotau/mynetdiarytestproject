package com.mynetdiary.testproject.clientdetails.view

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayoutMediator
import com.mynetdiary.testproject.R
import com.mynetdiary.testproject.clientdetails.view.dateofbirth.DateOfBirthFragment
import com.mynetdiary.testproject.clientdetails.view.photo.PhotoFragment
import com.mynetdiary.testproject.clientdetails.view.weight.WeightFragment
import com.mynetdiary.testproject.databinding.FragmentClientDetailsBinding
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class ClientDetailsFragment : Fragment(R.layout.fragment_client_details) {

    private val args: ClientDetailsFragmentArgs by navArgs()

    private val clientDetailsSharedViewModel by sharedViewModel<ClientDetailsSharedViewModel>()
    private val clientDetailsViewModel by viewModel<ClientDetailsViewModel> {
        parametersOf(args.client, clientDetailsSharedViewModel)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as? AppCompatActivity)?.supportActionBar?.title = ""

        val binding = FragmentClientDetailsBinding.bind(view)
        binding.viewModel = clientDetailsViewModel

        val fragments = listOf(WeightFragment.newInstance(),
            DateOfBirthFragment.newInstance(),
            PhotoFragment.newInstance())

        binding.viewPager.adapter = ClientDetailsAdapter(fragments, this)
        binding.viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                val supportActionBar = (activity as? AppCompatActivity)?.supportActionBar

                when (position) {
                    0 -> supportActionBar?.title = getString(R.string.title_weight)
                    1 -> supportActionBar?.title = getString(R.string.title_date_of_birth)
                    2 -> supportActionBar?.title = getString(R.string.title_photo)
                }
            }

        })
        TabLayoutMediator(binding.tabLayout, binding.viewPager) { tab, _ ->
            tab.view.isEnabled = false
        }.attach()
    }
}