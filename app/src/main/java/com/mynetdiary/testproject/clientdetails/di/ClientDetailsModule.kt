package com.mynetdiary.testproject.clientdetails.di

import androidx.navigation.NavController
import com.mynetdiary.testproject.clientdetails.view.ClientDetailsSharedViewModel
import com.mynetdiary.testproject.clientdetails.view.ClientDetailsViewModel
import com.mynetdiary.testproject.clientdetails.view.dateofbirth.DateOfBirthViewModel
import com.mynetdiary.testproject.clientdetails.view.photo.PhotoViewModel
import com.mynetdiary.testproject.clientdetails.view.weight.WeightViewModel
import com.mynetdiary.testproject.clients.view.ClientsSharedViewModel
import com.mynetdiary.testproject.model.Client
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val clientDetailsModule = module {
    viewModel { (client: Client, clientDetailsSharedViewModel: ClientDetailsSharedViewModel) ->
        ClientDetailsViewModel(
            client = client,
            clientDetailsSharedViewModel = clientDetailsSharedViewModel
        )
    }

    viewModel { (clientDetailsSharedViewModel: ClientDetailsSharedViewModel,
                    navController: NavController) ->
        WeightViewModel(
            clientDetailsSharedViewModel = clientDetailsSharedViewModel,
            resUtils = get(),
            navController = navController
        )
    }

    viewModel { (clientDetailsSharedViewModel: ClientDetailsSharedViewModel) ->
        DateOfBirthViewModel(
            clientDetailsSharedViewModel = clientDetailsSharedViewModel
        )
    }

    viewModel { (clientsSharedViewModel: ClientsSharedViewModel,
                    clientDetailsSharedViewModel: ClientDetailsSharedViewModel,
                    navController: NavController) ->
        PhotoViewModel(
            clientsSharedViewModel = clientsSharedViewModel,
            clientDetailsSharedViewModel = clientDetailsSharedViewModel,
            storageUtils = get(),
            navController = navController
        )
    }

    viewModel { ClientDetailsSharedViewModel() }
}