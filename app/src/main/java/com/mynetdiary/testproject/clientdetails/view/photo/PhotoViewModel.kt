package com.mynetdiary.testproject.clientdetails.view.photo

import android.net.Uri
import androidx.lifecycle.ViewModel
import androidx.navigation.NavController
import com.mynetdiary.testproject.clientdetails.view.ClientDetailsSharedViewModel
import com.mynetdiary.testproject.clients.view.ClientsSharedViewModel
import com.mynetdiary.testproject.core.utils.StorageUtils

class PhotoViewModel(val clientDetailsSharedViewModel: ClientDetailsSharedViewModel,
                     private val clientsSharedViewModel: ClientsSharedViewModel,
                     private val storageUtils: StorageUtils,
                     private val navController: NavController) : ViewModel() {

    fun onPhotoPicked(uri: Uri) {
        clientDetailsSharedViewModel.clientData.get()?.photoUri = storageUtils.copyImageToExternalAppStorage(uri).toString()
    }

    fun onPhotoTaken(uri: Uri) {
        clientDetailsSharedViewModel.clientData.get()?.photoUri = uri.toString()
    }

    fun onBackClick() {
        clientDetailsSharedViewModel.selectedPage.get()?.let { selectedPage ->
            clientDetailsSharedViewModel.selectedPage.set(selectedPage - 1)
        }
    }

    fun onNextClick() {
        val clients = clientsSharedViewModel.clientsData.get()?.toMutableList()

        clientDetailsSharedViewModel.clientData.get()?.let { client ->
            if (clients?.contains(client) == false) {
                clients.add(client)
                clientsSharedViewModel.clientsData.set(clients)
            }
        }

        navController.navigateUp()
    }
}