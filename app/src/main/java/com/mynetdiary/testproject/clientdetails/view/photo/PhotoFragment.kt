package com.mynetdiary.testproject.clientdetails.view.photo

import android.Manifest
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.PopupMenu
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.mynetdiary.testproject.R
import com.mynetdiary.testproject.clientdetails.view.ClientDetailsSharedViewModel
import com.mynetdiary.testproject.clients.view.ClientsSharedViewModel
import com.mynetdiary.testproject.core.utils.StorageUtils
import com.mynetdiary.testproject.databinding.FragmentPhotoBinding
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class PhotoFragment : Fragment(R.layout.fragment_photo) {

    companion object {
        fun newInstance() = PhotoFragment()
    }

    private lateinit var requestPermissionLauncher: ActivityResultLauncher<Array<String>>
    private lateinit var openDocumentLauncher: ActivityResultLauncher<Array<String>>
    private lateinit var takePictureLauncher: ActivityResultLauncher<Uri>

    private var newPhotoUri: Uri? = null

    private val storageUtils by inject<StorageUtils>()

    private val clientsSharedViewModel by sharedViewModel<ClientsSharedViewModel>()
    private val clientDetailsSharedViewModel by sharedViewModel<ClientDetailsSharedViewModel>()
    private val photoViewModel by viewModel<PhotoViewModel> {
        parametersOf(clientsSharedViewModel, clientDetailsSharedViewModel, findNavController())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initLaunchers()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val binding = FragmentPhotoBinding.bind(view)
        binding.viewModel = photoViewModel

        binding.photo.setOnClickListener {
            PopupMenu(requireContext(), it).apply {
                inflate(R.menu.menu_add_photo)
                setOnMenuItemClickListener { menuItem ->
                    when (menuItem.itemId) {
                        R.id.photo_library -> openDocumentLauncher.launch(arrayOf("image/*"))
                        R.id.photo_camera -> requestPermissionLauncher.launch(arrayOf(Manifest.permission.CAMERA,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE))
                    }
                    false
                }
            }.show()
        }
    }

    private fun initLaunchers() {
        openDocumentLauncher = registerForActivityResult(ActivityResultContracts.OpenDocument()) { selectedUri ->
            selectedUri?.let {
                photoViewModel.onPhotoPicked(it)
            }
        }

        requestPermissionLauncher = registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { isGrantedMap ->
            val allGranted = isGrantedMap.values.all { it }
            when {
                allGranted -> {
                    // Camera permission is granted
                    newPhotoUri = storageUtils.createExternalAppStorageImageFile()
                    takePictureLauncher.launch(newPhotoUri)
                }
                shouldShowRequestPermissionRationale(Manifest.permission.CAMERA) || shouldShowRequestPermissionRationale(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE) -> {
                    // Camera permission wasn't granted once
                    // TODO Show proper message dialog
                }
                else -> {
                    // Camera permission wasn't granted completely
                    // TODO Show proper message dialog
                }
            }
        }

        takePictureLauncher = registerForActivityResult(ActivityResultContracts.TakePicture()) { success ->
            if (success) {
                newPhotoUri?.let {
                    photoViewModel.onPhotoTaken(it)
                }
            }
        }
    }
}