package com.mynetdiary.testproject

import android.app.Application
import com.mynetdiary.testproject.clientdetails.di.clientDetailsModule
import com.mynetdiary.testproject.clients.di.clientsModule
import com.mynetdiary.testproject.core.di.utilsModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MyNetDiaryApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@MyNetDiaryApplication)
            modules(utilsModule, clientsModule, clientDetailsModule)
        }
    }
}